const path = require("path");
const Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const tsconfigPaths = require("./tsconfig.paths.json");

const f = str => str.replace("/*", "");

module.exports = {
  mode: "development",
  entry: {
    main: "./src/index.tsx",
  },
  output: {
    path: path.resolve(__dirname, './build'),
    publicPath: "/",
    filename: "[name].js",
  },
  resolve: {
    roots: [path.resolve(__dirname, './src')],
    extensions: [".ts", ".tsx", ".js"],
    alias: Object.entries(tsconfigPaths.compilerOptions.paths).reduce((acc, [key, val]) =>
      (acc[f(key)] = path.resolve(__dirname, "./src", "./"+f(val[0]))) && acc,
      {}
    ),
  },
  module: {
    rules: [
      { test: /\.d\.tsx?$/, loader: 'ignore-loader' },
      {
        test: /\.(j|t)sx?$/,
        exclude: [/\.d\.tsx?$/, /node_modules/],
        use: [
          {
            loader: "babel-loader",
            options: {
              sourceType: "module",
              presets: [
                ["@babel/preset-react", { runtime: "automatic" }],
                ["@babel/env", { targets: ["last 2 versions"] }],
              ],
              plugins: [
                "@babel/transform-runtime",
              ],
            }
          }, { loader: "ts-loader", options: { transpileOnly: true }}
        ],
      },
      {
        test: /\.s?(c|a)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader"
        ],
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/index.html",
    }),
    new Dotenv(),
    new MiniCssExtractPlugin(),
  ],
};
