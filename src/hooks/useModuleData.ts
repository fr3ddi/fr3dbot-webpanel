import { useEffect } from "react";
import server from "lib/server";
import useGuildData from "./useGuildData";

export default function useModuleData<Type>(module: string, method: string, ...args: any[]) {
  const [data, update] = useGuildData<Type>(module+"."+method, ...args);

  useEffect(() => {
    server.socket.on("moduleUpdate-"+module, update);

    return () => { server.socket.off("moduleUpdate-"+module, update); };
  }, [update]);

  return [data, update] as [Type, ()=>void];
}
