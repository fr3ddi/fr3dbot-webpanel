import {useEffect, useState, useCallback} from "react";

export default function usePromise<Type>(func: ()=>Promise<Type>, autotrigger=true) {
  const [data, setData] = useState<Type>(null);

  const update = useCallback(() => { func().then(setData); }, [func]);
  useEffect(() => autotrigger && update(), [update]);

  return [data, update] as [Type, ()=>void];
}
