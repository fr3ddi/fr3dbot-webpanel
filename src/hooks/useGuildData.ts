import { useRef, useEffect } from "react";
import useData from "./useData";
import useSharedState from "./useSharedState";

export default function useGuildData<Type>(method: string, ...args: any[]) {
  const [data, update] = useData<Type>(method, ...args);
  const firstRun = useRef(true);

  // If any of these update, we probably need to update as well
  const user = useSharedState("user");
  const guild = useSharedState("guild");

  useEffect(() => {
    if (!firstRun.current) return update();
    else firstRun.current = false;
  }, [user, guild, update]);

  return [data, update] as [Type, ()=>void];
}
