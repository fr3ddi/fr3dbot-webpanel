import {useState, useEffect} from "react";
import server from "lib/server";

export default function useSharedState<Type>(state: string) {
  const [value, setValue] = useState(server.getCached<Type>(state));

  useEffect(() => {
    server.get<Type>(state).then(setValue);

    server.watch(state, setValue);

    return () => server.unwatch(state, setValue);
  }, [state]);

  return value;
}
