import { useState, useEffect, useCallback } from "react";
import server from "lib/server";

export default function useData<Type>(method: string, ...args: any[]) {
  const [data, setData] = useState<Type>(server.getCachedData(method, ...args));

  useEffect(() => {
    const clear = server.listenData<Type>(method, setData, ...args);
    if (!data) server.call(method, ...args);
    return clear;
  }, [method, setData, args]);

  const update = useCallback<()=>Promise<Type>>(() => server.call(method, ...args), [method, args]);

  return [data, update] as [Type, ()=>void];
}
