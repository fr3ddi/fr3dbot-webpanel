import { Component, Suspense, lazy } from "react";
import { Helmet } from "react-helmet";
import { config, library, dom as fadom } from '@fortawesome/fontawesome-svg-core';
import { faSignOutAlt, faAngleDown, faAngleUp, faAngleLeft, faSpinner, faCog, faBars, faTimes, faPlus, faTrash, faSync, faHashtag, faVolumeUp, faBullhorn, faUpload, faEllipsisV, faEdit, faExclamationTriangle, faLaugh } from '@fortawesome/free-solid-svg-icons';
import { faDiscord, faBitbucket } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Toaster, toast } from "react-hot-toast";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import server, { SharedState } from "lib/server";
import appInit from "lib/appInit";
import Header from "$partials/Header";

import "styles/global.scss";

const GuildSelector = lazy(() => import("$partials/GuildSelector"));
const Dashboard = lazy(() => import("pages/Dashboard"));
const BotInfo = lazy(() => import("pages/BotInfo"));
const ModuleRouter = lazy(() => import("pages/ModuleRouter"));


config.autoAddCss = false;
config.keepOriginalSource = false;
config.observeMutations = false;
library.add(faSignOutAlt, faAngleDown, faAngleUp, faAngleLeft, faSpinner, faCog, faBars, faTimes, faPlus, faTrash, faSync, faHashtag, faVolumeUp, faBullhorn, faUpload, faEllipsisV, faEdit, faExclamationTriangle, faLaugh);
library.add(faDiscord, faBitbucket);

const BASE_URL = process.env.REACT_APP_FRONTEND_URL+"/";
const toastOptions = {
  style: {
    background: "#23272A", // notblack
    color: "white",
    maxWidth: "min(90vw, 512px)",
    minWidth: "min(90vw, 256px)",
  },
  duration: 7_500,
};

type AppState = {
  authUrl: string;
  user: SharedState["user"];
  guild: SharedState["guild"];
};

export default class App extends Component {
  readonly BASE_URL = BASE_URL;
  state: AppState = {
    authUrl: null,
    user: null,
    guild: null,
  };
  connectError = false;
  dataAwaitToastID: string;

  componentDidMount() {
    appInit.bind(this)();

    server.socket.on("disconnect", () => {
      this.setState({ authUrl: null, user: null, guild: null });
    });

    server.connectStates(["user", "guild"], this);
  }

  render() {
    const { authUrl, user, guild } = this.state;

    if (this.dataAwaitToastID && user) {
      toast.success("Session is ready", { id: this.dataAwaitToastID });
      this.dataAwaitToastID = null;
    }

    return (
      <Router>
        <Helmet>
          <style>{ fadom.css().replace(/(?<![a-z0-9])[ \n]+/gi, "") }</style>
        </Helmet>

        <Toaster position="bottom-left" reverseOrder toastOptions={toastOptions} />

        <div className="min-h-screen flex flex-col">
          <Header user={ user } guild={ guild } />

          <main className="flex flex-grow flex-col justify-center items-center text-4xl p-2">
            <Suspense fallback={ "Loading" }>
              { !authUrl && !user ?
                // Connection to the server has not been estabilished yet
                "Connecting..."
              :
                <Switch>
                  <Route path="/bot" component={BotInfo} />
                  <Route path="/">
                    { user && !guild ?
                      // Guild Selection
                      <GuildSelector guilds={user.guilds} />
                    : this.state.authUrl ?
                      // Authorization required
                      <a href={authUrl} className="p-4 bg-blurple text-xl font-bold rounded-lg align-middle">
                        <FontAwesomeIcon icon={["fab", "discord"]} size="lg" className="mr-2" />{" "}
                        Login with Discord
                      </a>
                      :
                      // All ready
                      <Switch>
                        {/* NOTE: we can add `key={this.state.user.tag+this.state.guild}` to <Switch> above to force re-render on guild/user change, but useGuildData handles that on component level */}
                        <Route exact path="/" component={Dashboard} />
                        <Route exact path="/modules"><Redirect to="/" /></Route>

                        <Route path="/modules/:module" component={ModuleRouter} />
                      </Switch>
                    }
                  </Route>
                </Switch>
              }
            </Suspense>
          </main>

          <footer className="mt-4 p-2 flex justify-around flex-wrap">
            <div>
              This site is not associated with Discord.
            </div>
            <div>
              <a href="https://bitbucket.org/fr3ddi/fr3dbot-webpanel" target="_blank" rel="noreferrer" className="text-link hover:underline">
                <FontAwesomeIcon icon={["fab", "bitbucket"]} />{" "}
                View source code on Bitbucket
              </a>
            </div>
          </footer>
        </div>
      </Router>
    );
  }
}
