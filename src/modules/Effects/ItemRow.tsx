import { useState, useEffect } from "react";
import { toast } from "react-hot-toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Input from "$elements/Input";
import Select from "$elements/Select";
import server, { DelayedCall, EffectItem } from "lib/server";

export default function ItemRow({item}: {item: EffectItem}) {
  const [ transforms ] = server.effects.useTransforms();
  const [ changePromise, setChangePromise ] = useState<DelayedCall<boolean>>(null);

  function onChange(field: string, value: any) {
    const _item = { ...item, [field]: value };
    const promise = server.effects.edit(item.id, _item.pattern, _item.response);
    if (promise.retouched) return;
    setChangePromise(promise);
    promise.then(() => {
      setChangePromise(null);
      toast.success("Successfully saved changes");
    });
  }

  useEffect(() => () => { if (changePromise) changePromise.forceTrigger(); });

  return (
    <tr className="flex flex-wrap md:table-row">
      <td className="px-2 text-center">{ item.id }</td>
      <td className="flex-grow w-1/2">
        <Input key="pattern" type="text" wrapClassName="w-full" required value={item.pattern}
          onChange={ value => onChange("pattern", value) }
        />
      </td>
      <td className="flex-grow w-1/2">
        <Input key="response" type="text" wrapClassName="w-full" required value={item.response}
          onChange={ value => onChange("response", value) }
        />
      </td>
      <td className="flex-grow w-1/2">
        <Select key="response" className="w-full" value={item.response} forceValue placeholder="Transform"
          onChange={ value => onChange("response", value) } options={ transforms ? transforms.reduce((acc,i)=>{ acc[i] = i; return acc; }, {}) : {} }
        />
      </td>
      <td className="px-2">
        <button key="remove" className="px-2 rounded bg-gray-500 hover:bg-red-500"
          onClick={ () => server.effects.remove(item.id) }
        >
          <FontAwesomeIcon icon={["fas", "trash"]} />
        </button>
      </td>
    </tr>
  );
}
