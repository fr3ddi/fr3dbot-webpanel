import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Input from "$elements/Input";
import server from "lib/server";

export default function NewItem() {
  const [ pattern, setPattern ] = useState(null);
  const [ response, setResponse ] = useState(null);

  function create() {
    if (!pattern || !response) return;
    server.effects.add(pattern, response).then(() => {
      setPattern(null);
      setResponse(null);
    });
  }

  return (
    <div className="w-full max-w p-2 rounded bg-notblack mb-4 text-lg flex flex-wrap items-center">
      <Input type="text" required value={ pattern } placeholder="Pattern"
        onChange={ setPattern }
      />

      <Input type="text" className="mx-2" required value={ response } placeholder="Response"
        onChange={ setResponse }
      />

      <button className="ml-4 rounded transition bg-green-500 hover:bg-green-400 px-3 py-1 disabled:opacity-75"
        disabled={ !pattern || !response }
        onClick={ create }
      >
        <FontAwesomeIcon icon={["fas", "plus"]} /> Add effect
      </button>
    </div>
  );
}
