import server from "lib/server";
import ItemRow from "./ItemRow";
import NewItem from "./NewItem";

export default function Effects() {
  const [items] = server.effects.useList();

  return (
    <>
      <NewItem />

      <table className="w-full max-w text-lg rounded py-2 bg-notblack">
        <thead>
          <tr className="flex flex-wrap md:table-row">
            <th className="py-2 px-3 ">ID</th>
            <th className="py-2 px-3 w-1/2 flex-grow">Pattern</th>
            <th className="py-2 px-3 w-1/2 flex-grow">Response</th>
            <th className="py-2 px-3 "></th>
          </tr>
        </thead>
        <tbody>
          { items && items.length ?
            items.map(effect => <ItemRow key={effect.id} item={effect} />)
          :
            <tr><td colSpan={4} className="text-center p-2 text-gray-400">No effects yet</td></tr>
          }
        </tbody>
      </table>
    </>
  );
}
