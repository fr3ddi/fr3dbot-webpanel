import { lazy } from "react";

const Modules = {
  alerts: lazy(() => import("./Alerts")),
  effects: lazy(() => import("./Effects")),
  reactroles: lazy(() => import("./ReactRoles")),
};

export default Modules;
