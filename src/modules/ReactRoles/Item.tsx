import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import type { match as Match } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "react-hot-toast";
import server, { DelayedCall } from "lib/server";
import Input from "$elements/Input";
import Switch from "$elements/Switch";
import Dropdown from "$elements/Dropdown";
import Modal from "$elements/Modal";
import RoleSelect from "$elements/RoleSelect";
import { ModuleMatchParams } from "pages/ModuleRouter";
import RoleRow from "./RoleRow";

type ReactRolesItemMatchParams = ModuleMatchParams & {
  id: string,
};

type ReactRolesItemMatch = Match<ReactRolesItemMatchParams>;

export default function ReactRolesItem({ match }: { match: ReactRolesItemMatch }) {
  const [ changePromise, setChangePromise ] = useState<DelayedCall<boolean>>(null);
  const [ changingID, setChangingID ] = useState(false);
  const [ item ] = server.reactroles.useItem(Number(match.params.id));
  const history = useHistory();

  function goBack() {
    history.push(match.url.replace(/\/\w+\/$/, ""));
  }

  async function destroy() {
    await server.reactroles.destroy(item.id);
    toast.success("Removed react-roles item");
    goBack();
  }

  function onChange(field: string, value: any) {
    const promise = server.reactroles.edit(item.id, field, value);
    if (promise.retouched) return;
    setChangePromise(promise);
    promise.then(() => {
      setChangePromise(null);
      toast.success("Successfully saved changes");
    });
  }

  async function rerender() {
    if (changePromise) await changePromise.forceTrigger();
    await server.reactroles.render(item.id);
    toast.success("React-roles message (re)sent");
  }

  useEffect(() => () => { if (changePromise) changePromise.forceTrigger(); });

  return (
    <div className="w-full max-w">
      { changingID &&
        <Modal close={ () => setChangingID(false) }>
          <Input label="Message ID:" type="text" wrapClassName="w-full my-2 px-2 text-lg" value={item.messageID} note={
            <>
              <FontAwesomeIcon icon={["fas", "exclamation-triangle"]} className="mr-2" />
              Do NOT edit message ID unless you know what you're doing.
            </>
          } onChange={ (value) => onChange("messageID", value) } />
        </Modal>
      }

      <header className="flex flex-wrap mb-4">
        <h1 className="mb-2">React-roles item { item ? item.id : "not found" }</h1>

        { item &&
          <div className="text-lg ml-auto">
            <button className="px-3 py-1 ml-1 rounded transition bg-green-600 hover:bg-green-500 transition"
              onClick={ rerender }
            >
              <FontAwesomeIcon icon={["fas", item.messageID ? "sync" : "upload"]} className="mr-3" />
              { item.messageID ? "Resend" : "Send" } message
            </button>

            <Dropdown toLeft className="inline-block" trigger={
              <button className="px-3 py-1 ml-1 rounded transition bg-gray-500 hover:bg-gray-400">
                <FontAwesomeIcon icon={["fas", "ellipsis-v"]} />
              </button>
            } menu={[
              <button className="px-3 py-1 w-full text-left" onClick={ () => setChangingID(true) }>
                <FontAwesomeIcon icon={["fas", "edit"]} fixedWidth className="mr-3"/>
                Change messageID
              </button>,
              <button className="px-3 py-1 hover:bg-red-500 w-full text-left" onClick={ destroy }>
                <FontAwesomeIcon icon={["fas", "trash"]} fixedWidth className="mr-3"/>
                Delete
              </button>,
            ]} />
          </div>
        }
      </header>

      { item &&
        <div className="text-lg flex flex-wrap">
          <Input label="Color:" type="color" wrapClassName="w-1/4 my-2 px-2" value={"#"+item.color.toString(16)}
            onChange={ (value) => onChange("color", value) }
          />
          <Input label="Title:" type="text" wrapClassName="w-3/4 my-2 px-2" value={item.title} placeholder="Choose your role"
            onChange={ (value) => onChange("title", value) }
          />

          <Input label="Message:" type="textarea" wrapClassName="px-2 my-2 w-full" spellCheck={true} hideable value={item.message}
            onChange={ (value) => onChange("message", value) } note={
              "Message to send alongside the embed, you can ping @everyone "+
              "and channels using <#ID>."
            }
          />

          <Input label="Description:" type="textarea" wrapClassName="px-2 my-2 w-full" hideable spellCheck={true} value={item.description}
            onChange={ (value) => onChange("description", value) }
          />

          <Switch label="Show help" className="my-4 px-4 sm:pl-2 sm:pr-6 sm:w-1/3 flex justify-between" value={item.showHelp}
            onChange={ (value) => onChange("showHelp", value ? "yes": "no") }
          />
          <Switch label="Show ID" className="my-4 px-4 sm:px-6 sm:w-1/3 flex justify-between" value={item.showID}
            onChange={ (value) => onChange("showID", value ? "yes": "no") }
          />
          <Switch label="Show roles" className="my-4 px-4 sm:pr-2 sm:pl-6 sm:w-1/3 flex justify-between" value={item.showRoles}
            onChange={ (value) => onChange("showRoles", value ? "yes": "no") }
          />

          <h2 className="text-2xl w-full mt-4">Roles</h2>
          <table className="w-full mx-2 my-2">
            <tbody>
              { item.roles.map((role, i) =>
                <RoleRow key={role ? role.id : "new"} itemID={item.id} i={i} item={role} />
              ) }
              <tr>
                <td colSpan={4} className="pt-2">
                  <RoleSelect placeholder="+ Add a role" className="w-full" forceValue
                    onChange={ role => server.reactroles.addRole(item.id, role) }
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      }
    </div>
  );
}
