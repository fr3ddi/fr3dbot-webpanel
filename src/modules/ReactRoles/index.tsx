import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Switch, Route, useHistory } from "react-router-dom";
import { toast } from "react-hot-toast";
import server from "lib/server";
import Modal from "$elements/Modal";
import TableList from "$elements/TableList";
import ChannelSelect from "$elements/ChannelSelect";
import { ModuleMatch } from "pages/ModuleRouter";
import Item from "./Item";


export default function ReactRoles({ match }: { match: ModuleMatch }) {
  const [ createModal, setCreateModal ] = useState(false);
  const [items] = server.reactroles.useList();
  const history = useHistory();

  function goToItem(id: number) {
    history.push(match.url.replace(/\/$/, "")+"/"+id+"/");
  }

  async function createItem(channelID: string) {
    const id = await server.reactroles.create(channelID);
    toast.success("Created new react-roles item");
    goToItem(id);
    setCreateModal(false);
  }

  return (
    <Switch>
      <Route exact path={ match.path }>
        { createModal &&
          <Modal close={ () => setCreateModal(false) }>
            <h2 className="text-2xl">Select a channel for new react-roles item</h2>
            <ChannelSelect type="text" required className="w-full my-4" onChange={ (value) => createItem(value) } />
          </Modal>
        }

        <div className="mb-4 w-full max-w text-base" onClick={ () => setCreateModal(true) }>
          <button className="rounded transition bg-green-500 hover:bg-green-400 px-3 py-2">
            <FontAwesomeIcon icon={["fas", "plus"]} /> Create new
          </button>
        </div>

        <TableList className="w-full max-w text-lg" header={["ID", "Title", "Channel"]}
          body={items && items.map(item => ({
            onClick: () => goToItem(item.id),
            className: "cursor-pointer",
            row: [
              item.id,
              item.title,
              item.channelName ? "#"+item.channelName : "-",
            ]
          }))}
        />
      </Route>

      <Route exact path={ match.path+"/:id" } component={Item} />
    </Switch>
  );
}
