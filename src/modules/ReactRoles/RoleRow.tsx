import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "react-hot-toast";
import { ReactrolesItemRole } from "lib/server/modules/reactroles";
import server, { DelayedCall } from "lib/server";
import Input from "$elements/Input";
import RoleSelect from "$elements/RoleSelect";
import EmojiSelector from "$elements/EmojiSelector";

const empty: ReactrolesItemRole = {
  id: null,
  emoji: null,
  role: null,
};

export default function RoleRow({ itemID, item: _item }: { itemID: number, i: number, item?: ReactrolesItemRole }) {
  const [ changePromise, setChangePromise ] = useState<DelayedCall<number>>(null);
  const [ item, setItem ] = useState<ReactrolesItemRole>(_item ?? { ...empty });

  useEffect(() => setItem(item), [_item]);

  const onChange = (field: string, value: any) => {
    item[field] = value;
    setItem(item);
    if (!item.role) return null;
    const promise = server.reactroles.setRole(
      itemID,
      item.id,
      item.role,
      item.emoji,
      item.description,
    );
    if (promise.retouched) return;
    setChangePromise(promise);
    promise.then(() => {
      setChangePromise(null);
      toast.success("Successfully saved changes");
    });
  };

  useEffect(() => () => { if (changePromise) changePromise.forceTrigger(); });

  const remove = () => {
    if (item.id) server.reactroles.removeRole(itemID, item.id);
  };

  return (
    <tr className="flex flex-wrap md:table-row">
      <td className="w-auto pr-1 py-1"><EmojiSelector
        selected={ item.emoji }
        setSelected={ emoji => onChange("emoji", emoji) }
      /></td>
      <td className="flex-grow md:w-1/3"><RoleSelect placeholder="Select a role" className="w-full"
        value={ item.role } required
        onChange={ value => onChange("role", value) }
      /></td>
      <td className="flex-grow md:w-2/3"><Input placeholder="Role description (optional)" wrapClassName={"h-11 w-full "+(item.emoji ? "" : "invisible")}
        value={ item.description }
        onChange={ value => onChange("description", value) }
      /></td>
      <td className="w-auto">{ item.id && <button className="p-2 transition text-gray-400 hover:text-red-400" onClick={ remove }>
        <FontAwesomeIcon icon={["fas", "trash"]} size="lg" />
      </button> }</td>
    </tr>
  );
}
