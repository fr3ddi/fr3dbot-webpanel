import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import server from "lib/server";
import NewItem from "./NewItem";

export default function Alerts() {
  const [items] = server.alerts.useList();

  return (
    <>
      <NewItem />

      <table className="w-full max-w text-lg rounded py-2 bg-notblack">
        <thead>
          <tr className="flex flex-wrap md:table-row">
            <th className="py-2 px-3">Type</th>
            <th className="py-2 px-3 w-1/2 flex-grow">Name</th>
            <th className="py-2 px-3 w-1/4">Channel</th>
            <th className="py-2 px-3 w-1/3 flex-grow">Role</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          { items && items.length ?
            items.map((item, i) =>
              <tr key={i}>
                { Object.values(item).map((column, j) =>
                  <td key={j} className="text-center p-2">{ column || "-" }</td>
                ) }
                <td className="p-2">
                  <button className="px-2 rounded bg-gray-500 hover:bg-red-500"
                    onClick={ () => server.alerts.unsubscribe(item.type, item.name) }
                  >
                    <FontAwesomeIcon icon={["fas", "trash"]} />
                  </button>
                </td>
              </tr>
            )
          :
            <tr><td colSpan={5} className="text-center p-2 text-gray-400">No alerts yet</td></tr>
          }
        </tbody>
      </table>
    </>
  );
}
