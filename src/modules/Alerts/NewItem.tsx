import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from "$elements/Select";
import ChannelSelect from "$elements/ChannelSelect";
import RoleSelect from "$elements/RoleSelect";
import Input from "$elements/Input";
import server from "lib/server";

export default function NewItem() {
  const [ types ] = server.alerts.useTypes();
  const [ type, setType ] = useState(null);
  const [ name, setName ] = useState(null);
  const [ channel, setChannel ] = useState(null);
  const [ role, setRole ] = useState(null);

  function create() {
    if (!type || !name || !channel) return;
    server.alerts.subscribe(type, name, channel, role).then(() => {
      setType(null);
      setName(null);
      setChannel(null);
      setRole(null);
    });
  }

  return (
    <div className="w-full max-w p-2 rounded bg-notblack mb-4 text-lg flex flex-wrap items-center">
      <Select required value={ type } placeholder="Type" className="mr-2"
        onChange={ setType } options={ (types ?? []).reduce((acc, item) => { acc[item] = item; return acc; }, {}) }
      />

      <Input type="text" wrapClassName="mr-2" required value={ name } placeholder="Name"
        onChange={ setName }
      />

      <ChannelSelect type={["text", "news"]} className="mr-2" required value={ channel } placeholder="Channel"
        onChange={ setChannel }
      />

      <RoleSelect className="mr-2" value={ role } placeholder="Role to ping"
        onChange={ setRole }
      />

      <button className="rounded transition bg-green-500 hover:bg-green-400 px-3 py-1 disabled:opacity-75"
        disabled={ !name || !type || !channel }
        onClick={ create }
      >
        <FontAwesomeIcon icon={["fas", "plus"]} /> Add alert
      </button>
    </div>
  );
}
