import { Suspense } from "react";
import { match as Match } from "react-router-dom";
import server from "lib/server";
import ModuleHeader from "$elements/ModuleHeader";
import Modules from "modules/index";

export type ModuleMatchParams = {
  module: string,
};

export type ModuleMatch = Match<ModuleMatchParams>;

export default function ModuleRouter({ match }: { match: ModuleMatch }) {
  const [ modules ] = server.bot.useModules();
  const { module: moduleId } = match.params;

  const module = modules && modules.find(_module => _module.id === moduleId);
  const ModuleComponent = Modules[moduleId];

  return (
    <>
      { module && match.isExact &&
        <header className="mb-4 p-2 md:p-4 w-full rounded bg-notblack max-w">
          <ModuleHeader module={module} />
        </header>
      }

      { module && ModuleComponent &&
        <Suspense fallback="Loading...">
          <ModuleComponent match={match} />
        </Suspense>
      }

      <div className="mb-auto" />
    </>
  );
}
