import { useState, useEffect, Fragment } from "react";
import moment from "moment";
import server from "lib/server";
import OptionalImage from "$elements/OptionalImage";

export default function BotInfo() {
  const [bot] = server.bot.useBotData();
  const [ uptime, setUptime ] = useState("");

  useEffect(() => {
    if (!bot) return;

    const update = () => bot && setUptime(moment(bot.readyTimestamp).toNow(true));
    update();
    const interval = setInterval(update, 10_000);

    return () => {
      if (interval) clearInterval(interval);
    };
  }, [bot]);

  return (bot ?
    <div className="text-center flex flex-col items-center" style={{ maxWidth: 780 }}>
      <div className="inline-flex text-left mt-16">
        <OptionalImage src={`https://cdn.discordapp.com/avatars/${bot.id}/${bot.avatar}.webp?size=${128}`}
          className="rounded-full mr-2" width={128} height={128} toggle={bot.avatar}
        />
        <div className="ml-4 flex justify-center flex-col flex-shrink">
          <h2>{ bot.tag }</h2>
          <span className="text-gray-500 text-lg">{ bot.id }</span>
        </div>
      </div>

      <div className="mt-4 text-gray-400">
        Running for <span className="text-white">{ uptime }</span>{" "}
        in a <span className="text-white">{ bot.production ? "production" : "development" }</span> mode{" "}
        as a member of <span className="text-white">{ bot.guilds || 0 }</span> guild(s).
      </div>

      <div className="mt-4 text-gray-400 text-lg">
        Loaded modules:{" "}
        <ul className="inline">{ (bot.modules as string[]).map((module, i) =>
          <Fragment key={module}>
            { i ? ", " : "" }<li className="inline text-white">{ module }</li>
          </Fragment>
        ) }</ul>
      </div>
    </div>
  :
    <>Loading...</>
  );
}
