import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import server from "lib/server";
import ModuleHeader from "$elements/ModuleHeader";

export default function Dashboard() {
  const [modules] = server.bot.useModules();

  return (
    <>
      Modules

      <div className="container w-full flex flex-wrap justify-center mb-auto">
        { modules && modules.map(module =>
          <Link to={"/modules/"+module.id} key={module.id} className="block p-2 m-2 w-full lg:max-w-1/3 md:max-w-1/2 sm:max-w-full rounded bg-notblack flex">
            <div className="px-2 py-1 text-lg">
              <FontAwesomeIcon icon={["fas", "cog"]} />
            </div>
            <div className="flex-grow pb-1 text-xl">
              <ModuleHeader module={module} />
            </div>
          </Link>
        ) }
      </div>
    </>
  );
}
