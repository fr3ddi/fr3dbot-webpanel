import io, { Socket } from "socket.io-client";
import EventEmitter from "eventemitter3";
import DelayedCall from "./DelayedCall";

import alerts from "./modules/alerts";
import auth from "./modules/auth";
import bot from "./modules/bot";
import effects from "./modules/effects";
import reactroles from "./modules/reactroles";

type Callback<Type> = (...args: Type[]) => void;

let onConnect: ()=>void;

class ServerAPI extends EventEmitter {
  alerts = alerts;
  auth = auth;
  bot = bot;
  effects = effects;
  reactroles = reactroles;

  private readonly state = new Map<string, any>();
  private readonly cache = new Map<string, any>();
  private readonly delays = new Map<string, DelayedCall<any>>();
  private readonly debounce = new Map<string, Promise<any>>();
  socket: Socket;
  connectPromise: Promise<void> = new Promise(resolve => onConnect = resolve);

  init(): void {
    const url = new URL(process.env.REACT_APP_BACKEND_URL);
    this.socket = io(url.origin, {
      path: url.pathname.replace(/\/$/, "")+"/socket",
      transports: ["websocket", "polling"],
      query: {
        sessionID: window.localStorage.getItem("sessionID") || "",
      },
    });

    this.socket.on("connect", () => {
      onConnect();
    });

    this.socket.on("disconnect", () => {
      this.connectPromise = new Promise(resolve => onConnect = resolve);
    });

    this.socket.on("sessionID", (sessionID: string) => {
      window.localStorage.setItem("sessionID", sessionID);
    });
  }

  private hashCall(prop: string, ...args: any[]) {
    return JSON.stringify([prop, ...args]); // NOTE: There might be a better hashing method out there?
  }

  async _waitFor<Type>(...data: any[]): Promise<Type> {
    const eventName: string = data.shift();
    const hash = this.hashCall(eventName, ...data);

    // If this exact command is already running at this moment, don't rerun in
    if (this.debounce.has(hash)) return this.debounce.get(hash);

    const promise = new Promise<Type>(resolve => this.connectPromise.then(() =>
      this.socket.emit(eventName, ...data, (response: Type) => {
        this.debounce.delete(hash);

        resolve(response);

        // Trigger events only for do methods and only if the result changed
        if (eventName.startsWith("do") && JSON.stringify(this.cache.get(hash)) !== JSON.stringify(response)) {
          this.emit(hash, response);
          this.cache.set(hash, response);
        }
      })
    ));
    this.debounce.set(hash, promise);

    return promise;
  }

  async call<Type>(prop: string, ...args: any[]) {
    return this._waitFor<Type>(toMethod("do", prop), ...args);
  }

  slowCall<Type>(delay: number, prop: string, ...args: any[]): DelayedCall<Type> {
    let delayedCall = this.delays.get(prop);

    if (!delayedCall) {
      delayedCall = new DelayedCall<Type>(
        delay,
        () => this.delays.delete(prop),
        (..._args) => this._waitFor<Type>(toMethod("do", prop), ..._args)
      );

      this.delays.set(prop, delayedCall);
    }

    delayedCall.touch(args);

    return delayedCall;
  }

  getCachedData<Type>(prop: string, ...args: any[]) {
    return this.cache.get(this.hashCall(toMethod("do", prop), ...args)) as Type;
  }

  listenData<Type>(prop: string, callback: Callback<Type>, ...args: any[]): ()=>void {
    const method = toMethod("do", prop);
    const hash = this.hashCall(method, ...args);

    this.on(hash, callback);

    return () => {
      this.off(hash, callback);
    };
  }

  getCached<Type>(prop: string) { return this.cache.get(prop) as Type; }

  async get<Type>(prop: string) {
    if (this.state.has(prop)) return this.state.get(prop) as Type;

    const value: Type = await this._waitFor<Type>(toMethod("get", prop));
    if (value !== null) this.registerProp(prop, value);
    return value;
  }

  async set(prop: string, value: any) {
    return this._waitFor(toMethod("set", prop), value);
  }

  registerProp(prop: string, initialValue?: any) {
    this.state.set(prop, initialValue);
    this.watch(prop, (value: any) => this.state.set(prop, value));
  }

  watch<Type>(prop: string, callback: Callback<Type>) {
    this.socket.on(toMethod("changed", prop), callback);
  }
  unwatch<Type>(prop: string, callback: Callback<Type>) {
    this.socket.off(toMethod("changed", prop), callback);
  }

  connectStates(states: string[], that: React.Component): ()=>void {
    const cleanups: (()=>void)[] = [];

    for (const state of states) {
      const update = (value: any) => that.setState({ [state]: value });
      if (this.state.has(state)) update(this.state.get(state));
      this.get(state); // Update asynchronously
      this.watch(state, update);
      cleanups.push(() => this.unwatch(state, update));
    }

    return () => cleanups.forEach(f => f());
  }

  connectData2States(state2method: Record<string, string>, that: React.Component) {
    const cleanups: (()=>void)[] = [];

    for (const [state, method] of Object.entries(state2method)) {
      const update = (value: any) => that.setState({ [state]: value });
      const cached = this.getCached<any>(method);
      if (cached) update(cached);

      cleanups.push(() => this.listenData(method, update));
    }

    return () => cleanups.forEach(f => f());
  }
}


function toMethod(type: string, prop: string): string {
  return type + (prop[0] || "").toUpperCase() + prop.substr(1);
}


export default new ServerAPI();
export { DelayedCall };
export * from "./SharedState.d";
export * from "./modules/alerts";
export * from "./modules/auth";
export * from "./modules/bot";
export * from "./modules/effects";
export * from "./modules/reactroles";
