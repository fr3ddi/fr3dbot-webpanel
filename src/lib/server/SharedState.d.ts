export interface SharedState {
  user: {
    username: string,
    locale: string,
    mfa_enabled: boolean,
    flags: number,
    avatar: string,
    discriminator: string,
    id: string,
    guilds: guildPreview[],
  };

  guildPreview: {
    id: string,
    name: string,
    icon: string,
    owner: boolean,
    permissions: number,
    permissions_new: number,
  };

  guild: string;
}
