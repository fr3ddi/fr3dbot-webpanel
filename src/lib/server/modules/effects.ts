import server, { DelayedCall } from "lib/server";
import useModuleData from "hooks/useModuleData";

export type EffectItem = {
  id: number,
  order: number,
  pattern: string,
  response: string,
};

const effectsAPI = {
  getList(): Promise<EffectItem[]> { return server.call("effects.list"); },
  useList() { return useModuleData<EffectItem[]>("effects", "list"); },

  getTransforms(): Promise<string[]> { return server.call("effects.transforms"); },
  useTransforms() { return useModuleData<string[]>("effects", "transforms"); },

  add(pattern: string, response: string): Promise<boolean> {
    return server.call("effects.add", pattern, response);
  },

  edit(id: number, pattern: string, response: string, delay=2000): DelayedCall<boolean> {
    return server.slowCall(delay, "effects.edit", id, pattern, response);
  },

  setTransform(id: number, transform: string) {
    return server.call("effects.transform", id, transform);
  },

  remove(...ids: number[]): Promise<boolean> {
    return server.call("effects.remove", ...ids);
  },
};
export default effectsAPI;
