import server, { DelayedCall } from "lib/server";
import useModuleData from "hooks/useModuleData";

export type ReactrolesItemPreview = {
  id: number;
  title: string;
  color: number;
  channelName: string;
};

export interface ReactrolesItem {
  id: number;
  guild: string;
  channelID?: string;
  messageID?: string;

  title?: string;
  color?: number;
  description?: string;
  message?: string;
  showHelp?: boolean;
  showID?: boolean;
  showRoles?: boolean;
  roles: ReactrolesItemRole[];
}

export interface ReactrolesItemRole {
  id: number;
  emoji: string;
  role: string;
  description?: string;
}

const reactrolesAPI = {
  getList(): Promise<ReactrolesItemPreview[]> { return server.call("reactroles.list"); },
  useList() { return useModuleData<ReactrolesItemPreview[]>("reactroles", "list"); },

  getItem(id: number): Promise<ReactrolesItem> { return server.call("reactroles.get", id); },
  useItem(id: number) { return useModuleData<ReactrolesItem>("reactroles", "get", id); },

  edit(id: number, field: string, value: any, delay=2000): DelayedCall<boolean> {
    return server.slowCall(delay, "reactroles.edit", id, field, value);
  },

  render(id: number): Promise<boolean> { return server.call("reactroles.render", id); },

  create(channelID: string): Promise<number> { return server.call("reactroles.create", channelID); },

  destroy(id: number): Promise<boolean> { return server.call("reactroles.destroy", id); },

  addRole(id: number, role: string): Promise<number> {
    return server.call("reactroles.addRole", id, role);
  },
  setRole(id: number, roleID: number, role: string, emoji?: string, description?: string, delay=2000): DelayedCall<number> {
    return server.slowCall(delay, "reactroles.setRole", id, roleID, role, emoji, description);
  },
  removeRole(id: number, roleID: number): Promise<void> {
    return server.call("reactroles.removeRole", id, roleID);
  },
};

export default reactrolesAPI;
