import server from "lib/server";
import useData from "hooks/useData";
import useGuildData from "hooks/useGuildData";

export type BotData = {
  id: string,
  avatar: string,
  tag: string,
  readyTimestamp: number,
  guilds: number,
  production: boolean,
  modules: string[],
};

export type Module = {
  id: string,
  name: string,
  description: string,
};

export type GuildChannelInfo = {
  id: string,
  name: string,
  type: string,
  position: number,
};

export type RoleInfo = {
  id: string,
  color: number,
  name: string,
};

export type GuildEmojis = {
  id: string;
  name: string;
  icon: string;
  emojis: {
    id: string;
    name: string;
    url: string;
    guild: string;
  }[];
};

const botAPI = {
  getBot(): Promise<BotData> { return server.call("getBot"); },
  useBotData() { return useData<BotData>("getBot"); },

  selectGuild(guildID: string) { return server.call<void>("selectGuild", guildID); },

  getModules(): Promise<Module[]> { return server.call("getModules"); },
  useModules() { return useGuildData<Module[]>("getModules"); },

  getChannels(type?: string|string[]): Promise<GuildChannelInfo[]> { return server.call("getChannels", type); },
  useChannels(type?: string|string[]) { return useGuildData<GuildChannelInfo[]>("getChannels", type); },

  getRoles(): Promise<RoleInfo[]> { return server.call("getRoles"); },
  useRoles() { return useGuildData<RoleInfo[]>("getRoles"); },

  getEmojis(): Promise<GuildEmojis[]> { return server.call("getAllGuildEmojis"); },
  useEmojis() { return useGuildData<GuildEmojis[]>("getAllGuildEmojis"); },
};

export default botAPI;
