import server from "lib/server";

interface PreauthData {
  authUrl: string;
}

const authAPI = {
  preauth(redirectUri: string): Promise<PreauthData|null> {
    return server.call("preauth", redirectUri);
  },

  auth(code: string, state: string, redirectUri: string): Promise<string> {
    return server.call("auth", code, state, redirectUri);
  },

  refresh(tokenCipher: string): Promise<string> {
    return server.call("refresh", tokenCipher);
  },

  logout() {
    server.socket.emit("logout");
    window.localStorage.clear();
    window.location.reload();
  },
};

export default authAPI;
