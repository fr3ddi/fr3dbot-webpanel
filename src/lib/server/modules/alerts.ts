import server from "lib/server";
import useModuleData from "hooks/useModuleData";

export type AlertsItem = {
  type: string,
  name: string,
  channel: string,
  role: string,
};

const alertsAPI = {
  getList(type?: string): Promise<AlertsItem[]> { return server.call("alerts.list", type); },
  useList(type?: string) { return useModuleData<AlertsItem[]>("alerts", "list", type); },

  getTypes(): Promise<string[]> { return server.call("alerts.types"); },
  useTypes() { return useModuleData<string[]>("alerts", "types"); },

  subscribe(type: string, name: string, channel: string, role?: string): Promise<boolean> {
    return server.call("alerts.subscribe", type, name, channel, role);
  },

  unsubscribe(type: string, name: string): Promise<boolean> {
    return server.call("alerts.unsubscribe", type, name);
  },
};
export default alertsAPI;
