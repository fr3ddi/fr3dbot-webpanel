import { toast } from "react-hot-toast";
import App from "App";
import server from "lib/server";

export default function appInit(this: App) {
  const query = new URLSearchParams(window.location.search);
  const qCode = query.get("code");
  const qState = query.get("state");
  if (qCode && qState) {
    // Remove the ugly data from url
    window.history.replaceState({}, document.title, window.location.pathname);
  }

  server.socket.on("connect", async () => {
    if (this.connectError) toast.success("Connection to server restored");
    this.connectError = false;

    // Ask for authorization url
    const preauth = await server.auth.preauth(this.BASE_URL);
    // Auth not needed
    if (!preauth) return;

    const refreshTokenCipher = window.localStorage.getItem("refreshTokenCipher");

    // Try to authorize
    if ((qCode && qState) || refreshTokenCipher) {
      const toastId = toast.loading("Authorizing...");
      let newRefreshTokenCipher = "";

      if (qCode && qState) {
        newRefreshTokenCipher = await server.auth.auth(qCode, qState, this.BASE_URL);
      } else {
        newRefreshTokenCipher = await server.auth.refresh(refreshTokenCipher);
      }

      if (!newRefreshTokenCipher) {
        toast.error("Authorization failed, please, try again", { id: toastId });
      } else {
        // Successfully (re)auth'd
        window.localStorage.setItem("refreshTokenCipher", newRefreshTokenCipher);
        toast.success("Successfully authorized", { id: toastId });
        this.dataAwaitToastID = toast.loading("Awaiting session data from server...");

        // Restore previous path
        const preAuthPath = window.sessionStorage.getItem("preAuthPath");
        if (preAuthPath) {
          window.history.pushState({}, document.title, preAuthPath);
          window.sessionStorage.removeItem("preAuthPath");
        }
        return;
      }
    }

    this.setState({ authUrl: preauth.authUrl });
    window.sessionStorage.setItem("preAuthPath", window.location.pathname);
  });


  server.socket.on("connect_error", (error) => {
    this.connectError = true;
    toast.error("Failed to connect to backend: "+error.message);
  });

}
