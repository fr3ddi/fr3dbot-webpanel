import OptionalImage from "$elements/OptionalImage";
import server from "lib/server";

export default function GuildSelector({ guilds }) {

  return (
    <>
      <h1 className="mb-4">Choose guild</h1>

      <ul className="flex flex-wrap justify-center">
        { Object.values(guilds).map((guild: any) =>
          <li key={guild.id} onClick={ () => server.bot.selectGuild(guild.id) }
            className="flex flex-col items-center m-1 p-2 rounded-lg cursor-pointer transition bg-transparent bg-notblack"
          >
            <OptionalImage src={`https://cdn.discordapp.com/icons/${guild.id}/${guild.icon}.webp?size=${128}`}
              className="rounded-lg overflow-hidden" width={128} height={128} toggle={ guild.icon }
            />

            <h2 className="mt-3 text-center text-lg leading-tight" style={{ width: 128 }}>
              { guild.name }
            </h2>
          </li>
        ) }
      </ul>
    </>
  );
}
