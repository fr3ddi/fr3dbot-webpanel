import { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cn from "classnames";
import server from "lib/server";
import OptionalImage from "$elements/OptionalImage";
import QuickGuildSelector from "./QuickGuildSelector";
import { useMediaQuery } from "@react-hook/media-query";

export default function Header({ user, guild }) {
  const { pathname } = useLocation();
  const desktop = useMediaQuery("only screen and (min-width: 599px)");
  const [ mobnavOpen, setMobnavOpen ] = useState(false);
  const [ bot ] = server.bot.useBotData();
  const currentGuild = (user && guild && user.guilds[guild]) || {};

  useEffect(() => {
    const docListener = () => mobnavOpen && setMobnavOpen(false);
    window.addEventListener("click", docListener, { passive: true });

    return () => {
      window.removeEventListener("click", docListener);
    };
  }, [mobnavOpen]);

  return (
    <div className="sticky top-0 z-40 bg-dark">
      <header className="relative p-1 text-xl flex justify-between items-center">
        <div className="p-3 flex items-center">
          <Link to=".." className={cn(
            "pl-2 pr-6 transition text-base",
            pathname === "/" ? "opacity-0 pointer-events-none" : "opacity-75 hover:opacity-100"
          )}>
            <FontAwesomeIcon icon={["fas", "angle-left"]} size="2x" />
          </Link>

          <Link to="/">
            <h1 className="text-2xl">Fr3dWebPanel</h1>
          </Link>

          { bot &&
            <Link to="/bot" className="p-1 px-2 ml-4 rounded-lg bg-notblack text-lg flex items-center hidden md:flex">
              <OptionalImage src={`https://cdn.discordapp.com/avatars/${bot.id}/${bot.avatar}.webp?size=${32}`}
                className="rounded-full mr-2" width={32} height={32} toggle={bot.avatar}
              />
              { bot.tag }
            </Link>
          }
        </div>

        {user &&
          <>
            <button className="py-2 px-4 text-3xl md:hidden" onClick={ () => setMobnavOpen(!mobnavOpen) }>
              <FontAwesomeIcon icon={["fas", mobnavOpen ? "times" : "bars"]} fixedWidth />
            </button>

            <div className={cn(
              "md:items-center md:flex-row md:static md:w-auto md:shadow-none md:py-0 md:transform-none",
              "absolute left-0 top-16 z-50 w-full flex flex-col items-stretch bg-dark shadow-lg py-4 transition transform-gpu origin-top",
              mobnavOpen ? "scale-y-1" : "scale-y-0",
            )}>
              <div className="order-1 md:order-none">
                { desktop ?
                  <QuickGuildSelector guilds={user.guilds} guild={ guild } />
                : guild &&
                  <div className="mx-3 text-lg">
                    <div className="px-2 py-1 bg-notblack rounded flex items-center">
                      <OptionalImage toggle={currentGuild.icon} src={`https://cdn.discordapp.com/icons/${currentGuild.id}/${currentGuild.icon}.webp?size=${16}`}
                        className="rounded-full mr-2" width={16} height={16} />
                      { currentGuild.name }
                    </div>
                    <button className="w-full p-2" onClick={ () => server.bot.selectGuild(null) }>
                      Change guild
                    </button>
                  </div>
                }
              </div>

              <div className="md:whitespace-nowrap md:ml-4 p-3 md:p-0 flex justify-between items-center">
                <span>{`${ user.username }#${ user.discriminator }`}</span>
                <button className="inline-block p-1 px-4 ml-2 md:mr-2 bg-red-700 hover:bg-red-600 transition rounded-lg text-lg"
                  onClick={ server.auth.logout }
                >
                  <FontAwesomeIcon icon={["fas", "sign-out-alt"]} className="mr-2" />
                  <span className="inline">Logout</span>
                </button>
              </div>
            </div>
          </>
        }
      </header>
    </div>
  );
}
