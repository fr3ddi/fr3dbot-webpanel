import Select, { SelectOptions } from "$elements/Select";
import OptionalImage from "$elements/OptionalImage";
import server from "lib/server";

export default function QuickGuildSelector({ guilds, guild }) {
  return (
    <Select slim value={ guild } forceValue options={Object.values(guilds).reduce<SelectOptions>((acc, {id, name, icon}) => {
      acc[id] = (
        <>
          <OptionalImage toggle={icon} src={`https://cdn.discordapp.com/icons/${id}/${icon}.webp?size=${16}`}
            className="rounded-full overflow-hidden mr-2" width={16} height={16} />
          <span className="flex-grow">{ name }</span>
        </>
      );
      return acc;
    }, {})} onChange={ server.bot.selectGuild } placeholder="Choose a guild" />
  );
}
