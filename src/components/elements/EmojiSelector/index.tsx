import { useState, useEffect, useRef, useMemo, Fragment } from "react";
import cn from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import OptionalImage from "$elements/OptionalImage";
import usePromise from "hooks/usePromise";
import server from "lib/server";
const getEmojis = ()=>import("./emojis.json");

interface EmojiFragment {
  url?: string;
  id?: string;
  name?: string;
}

interface EmojiSelectorProps {
  selected?: string;
  setSelected?: (emoji: string) => void;
}

export const renderEmoji = (emoji: EmojiFragment|string, size=32) => (
  <OptionalImage toggle src={ typeof emoji !== "string" ?
    `${emoji.url}?size=32` :
    `https://twemoji.maxcdn.com/2/svg/${Array.from(emoji).map((s:string)=>s.codePointAt(0).toString(16)).join("-")}.svg`}
    width={size} height={size} style={{ width: size, height: size }}
    className="rounded-full" mediaClass="object-contain"
  />
);

export default function EmojiSelector({ selected, setSelected }: EmojiSelectorProps) {
  const ID = useMemo(() => Math.random().toString(16).split(".")[1], []);
  const emojisRef = useRef<HTMLDivElement>(null);
  const [ open, setOpen ] = useState(false);
  const [ emojis, download ] = usePromise(getEmojis, false);
  const [ guildEmojis ] = server.bot.useEmojis();

  useEffect(() => {
    if (open) {
      download();

      const docListener = () => open && setOpen(false);
      window.addEventListener("click", docListener, { passive: true });

      return () => {
        window.removeEventListener("click", docListener);
      };
    }
  }, [open]);

  const select = (emoji: EmojiFragment|string) => {
    if (setSelected) setSelected(typeof emoji === "string" ? emoji : emoji.id);
    setOpen(false);
  };

  const scrollTo = (id: string) => {
    if (!emojisRef.current) return;
    const target = document.getElementById(id);
    target.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <div className="relative" onClick={ ev => ev.stopPropagation() }>
      <button className="p-2 transition text-gray-400 hover:text-white" onClick={ () => setOpen(!open) } style={{ fontSize: 22 }}>
        { selected && guildEmojis ?
          renderEmoji(guildEmojis.flatMap(g => g.emojis).find(i => i.id === selected) || selected, 22)
        :
          <FontAwesomeIcon icon={["fas", "laugh"]} />
        }
      </button>

      <div className={cn(
        "absolute left-full top-0 ml-1 rounded bg-notblack shadow-lg z-10 flex flex-col overflow-hidden w-80 h-64",
        "transform-gpu origin-top-left transition",
        !open && "scale-0",
      )}>
        <div className="p-2 shadow">
          <span>Choose an emoji</span>
        </div>
        <div className="flex h-full overflow-hidden">
          <div className="bg-blackish h-full w-9 py-3 p-2 overflow-y-auto">
            { !guildEmojis ?
              <FontAwesomeIcon icon={["fas", "spinner"]} pulse />
            :
              guildEmojis.map(({icon, id}) =>
                <div key={id} className="cursor-pointer" onClick={() => scrollTo(`EmojiSelector-${ID}-${id}-emojis`)}>
                  <OptionalImage toggle={icon} src={`https://cdn.discordapp.com/icons/${id}/${icon}.webp?size=${20}`}
                    className="rounded-full overflow-hidden" width={20} height={20} />
                </div>
              )
            }
            { !emojis ?
              <FontAwesomeIcon icon={["fas", "spinner"]} pulse />
            :
              <div className="cursor-pointer" onClick={() => scrollTo(`EmojiSelector-${ID}-default-emojis`)}>
                <FontAwesomeIcon icon={["fas", "laugh"]} />
              </div>
            }
          </div>

          <div className="w-full h-full p-2 leading-none overflow-y-auto" ref={emojisRef}>
            { !(guildEmojis && emojis && open) ? "Loading..." :
              <>
                { guildEmojis.map(({id, icon, name, emojis: _guildEmojis}) =>
                  <Fragment key={id}>
                    <div className="text-sm flex items-center my-1" id={`EmojiSelector-${ID}-${id}-emojis`}>
                      <OptionalImage toggle={icon} src={`https://cdn.discordapp.com/icons/${id}/${icon}.webp?size=${16}`}
                        className="rounded overflow-hidden mr-2" width={16} height={16} />
                      <span>{ name }</span>
                    </div>
                    { _guildEmojis.map(emoji =>
                      <div key={emoji.id} className="inline-block cursor-pointer m-0.5" onClick={ () => select(emoji) }>
                        { renderEmoji(emoji) }
                      </div>
                    ) }
                  </Fragment>
                ) }
                <div className="text-sm flex items-center my-1" id={`EmojiSelector-${ID}-default-emojis`}>
                  Default emojis
                </div>
                { Object.entries(emojis).map(emoji =>
                  <div key={emoji[0]} className="inline-block cursor-pointer m-0.5" onClick={ () => select(emoji[0]) }>
                    { renderEmoji(emoji[0]) }
                  </div>
                ) }
              </>
            }
          </div>
        </div>
      </div>
    </div>
  );
}
