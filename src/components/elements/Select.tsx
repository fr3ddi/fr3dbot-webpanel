import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cn from "classnames";

export type SelectOptions = (string|JSX.Element)[]|Record<string, string|JSX.Element>;

export type SelectProps = {
  label?: string,
  required?: boolean,
  options: SelectOptions,
  value?: string,
  forceValue?: boolean,
  slim?: boolean,
  placeholder?: string|React.Component,
  className?: string,
  onChange?: (key: string) => void,
};

// TODO: Add search
export default function Select({slim, placeholder, options, value, forceValue, onChange, label, required, className}: SelectProps) {
  const [ selected, _setSelected ] = useState(null);
  const [ open, setOpen ] = useState(false);

  const setSelected = (key: string) => {
    if (!forceValue) _setSelected(key);
    if (onChange) onChange(key);
  };

  useEffect(() => _setSelected(value), [value]);

  useEffect(() => {
    const docListener = () => open && setOpen(false);
    window.addEventListener("click", docListener, { passive: true });

    return () => {
      window.removeEventListener("click", docListener);
    };
  }, [open]);

  return (
    <label className={cn("block", className)}>
      { label && <span className="block font-normal leading-relaxed pl-2">
        { label }
        { required && <span className="text-red-500">*</span> }
      </span> }

      <div className={cn("relative block text-base text-left cursor-pointer", slim ? "h-8.5" : "h-11")}>
        <div onClick={ () => setOpen(!open) } className={cn(
          "bg-notblack border border-blackish rounded transition-rounded flex items-center",
          slim ? "p-1 h-8.5 px-2" : "p-2 px-3 h-11",
          { "rounded-b-none": !!open }
        )}>
          { selected && options[selected] ?
            options[selected]
          :
            <span className="text-gray-400">{placeholder || "Select an option"}</span>
          }
          <span className="ml-auto w-1" />
          { !required && selected && options[selected] &&
            <FontAwesomeIcon icon={["fas", "times"]} className="mx-1 transition text-gray-400 hover:text-white"
              onClick={ ev => { setSelected(null); ev.stopPropagation(); } }
            />
          }
          <FontAwesomeIcon icon={["fas", open ? "angle-up" : "angle-down"]} className={cn("ml-1", { "text-gray-400": !open})} />
        </div>
        <ul style={{ maxHeight: 256 }} className={cn(
          "relative z-10 w-full",
          "block py-2 -mt-px rounded-b bg-notblack border border-black transform-gpu transition origin-top overflow-y-auto",
          open ? "scale-y-1" : "scale-y-0"
        )}>
          { Object.entries(options).map(([key, _value]) => key !== selected &&
            <li key={key} className="p-1 px-2 pr-7 transition hover:bg-blackish" onClick={ () => setSelected(key) }>
              { _value }
            </li>
          ) }
        </ul>
      </div>
    </label>
  );
}
