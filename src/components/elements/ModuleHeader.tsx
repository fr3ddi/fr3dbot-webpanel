import { Module } from "lib/server";
export default function ModuleHeader({ module }: { module: Module }) {
  return (
    <>
      <div>{ module.name }</div>
      <div className="text-sm text-gray-400 leading-none">{ module.id }</div>
      <p className="text-base text-gray-200 leading-tight mt-2">{ module.description }</p>
    </>
  );
}
