import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import server from "lib/server";
import Select, { SelectProps } from "./Select";

type ChannelSelectProps = Omit<SelectProps, "options"> & {
  type?: string|string[],
};

const icons = {
  text: "hashtag",
  voice: "volume-up",
  news: "bullhorn",
};

export default function ChannelSelect({ type, ...props }: ChannelSelectProps) {
  const [channels] = server.bot.useChannels(type);

  return (
    <Select options={channels ? channels.reduce((acc, channel) => {acc[channel.id] = (
      <span>
        <FontAwesomeIcon icon={["fas", icons[channel.type]]} fixedWidth className="mr-2" />
        { channel.name }
      </span>
    ); return acc; }, {}) : []} {...props} />
  );
}
