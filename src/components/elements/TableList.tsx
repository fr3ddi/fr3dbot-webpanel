import cn from "classnames";

type TableListProps = {
  header?: any[],
  body?: ({
    onClick?: ()=>void,
    className?: string,
    row: any[],
  }|any[])[],
  emptyText?: string,
  className?: string,
};

export default function TableList({header, body, emptyText, className}: TableListProps) {
  return (
    <table className={cn("rounded bg-notblack", className)}>
      { header &&
        <thead className="border-b border-blackish">
          <tr>{ header.map(item => <th className="p-3 text-left" key={JSON.stringify(item)}>{ item }</th>) }</tr>
        </thead>
      }

      <tbody>
        { body && body.length ? body.map((row, i) =>
          <tr className={ "className" in row ? row.className : "" } key={i} onClick={ "onClick" in row ? row.onClick : ()=>null }>
            { ("row" in row ? row.row : row).map((item, j) =>
              <td className="py-2 px-3" key={j}>{ item || "-" }</td>
            ) }
          </tr>
        ) :
          <tr>
            <td colSpan={header ? header.length : 1} className="text-center italic text-base text-gray-300 p-4">
              { emptyText || "No items to show" }
            </td>
          </tr>
        }
      </tbody>

    </table>
  );
}
