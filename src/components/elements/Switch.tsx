import {useState, useEffect} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import cn from "classnames";

type SwitchOptions = {
  label?: string,
  value?: boolean,
  forceValue?: boolean,
  onChange?: (value: boolean) => void,
  className?: string,
};

export default function Switch({ label, value, forceValue, onChange, className }: SwitchOptions) {
  const [state, _setState] = useState(!!value);
  const [wantedState, setWanted] = useState(state);

  const setState = (_value: boolean) => {
    setWanted(_value);
    if (!forceValue) _setState(_value);
    if (onChange) onChange(_value);
    return true;
  };

  const inputEvent = ({target}) => {
    const _value = target.value !== "on";
    if (_value !== state) setState(_value);
  };

  useEffect(() => {
    _setState(value);
    setWanted(value);
  }, [value]);

  const stableState = wantedState === state; // if false, we're waiting for the value to be changed outside

  return (
    <label className={className}>
      { label && <span className="font-normal cursor-pointer leading-relaxed mr-2 whitespace-nowrap">{ label }</span> }
      <input type="checkbox" className="hidden" value={state?"on":"off"} onChange={ inputEvent } />

      <div onClick={ ev => stableState && setState(!state) && ev.preventDefault() } className={cn(
        "rounded-full h-7 w-12 p-1 transition cursor-pointer flex-shrink-0",
        state ? "bg-green-500" : "bg-gray-600",
      )}>
        <div className={cn("h-5 w-5 rounded-full transition p-0.5 bg-white relative transform", {"translate-x-full": state})}>
          <FontAwesomeIcon icon={["fas", "spinner"]} pulse className={cn(
            "text-gray-600 absolute inset-px transition delay-100",
            { "opacity-0": stableState }
          )} />
        </div>
      </div>
    </label>
  );
}
