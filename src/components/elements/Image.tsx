import { useState, useEffect, useMemo } from "react";
import cn from "classnames";

export interface ImageProps extends React.HTMLAttributes<HTMLImageElement> {
  src: string;
  alt?: string;
  onLoad?: () => void;
}

const empty = "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDUzLjYxNDAzNTA4NzcxOTMiIGhlaWdodD0iNDUzLjYxNDAzNTA4NzcxOTMiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+";

export default function Image({onLoad: aOnLoad, alt, src, ...props}: ImageProps) {
  const [ visible, setVisible ] = useState(false);
  const [ loaded, setLoaded ] = useState(false);
  const [ ref, setRef ] = useState<HTMLImageElement>(null);
  const observer = useMemo(() => new IntersectionObserver(([entry]) => {
    setVisible(entry.intersectionRatio > 0);
  }), []);

  useEffect(() => {
    if (ref) observer.observe(ref);
    return () => observer.disconnect();
  }, [ref, observer]);

  useEffect(() => {
    if (loaded && aOnLoad) aOnLoad();
  }, [loaded]);

  const onLoad = () => {
    if (ref && visible && ref.complete !== loaded) setLoaded(ref.complete);
  };
  onLoad();

  return (
    <img {...props} alt={alt || ""} ref={setRef} onLoad={onLoad} loading="lazy" decoding="async"
      className={cn("transition", !loaded ? "opacity-0" : "", props.className, !visible && "invisible")} src={ visible || loaded ? src : empty }
    />
  );
}
