import server from "lib/server";
import Select, { SelectProps } from "./Select";


export default function RoleSelect(props: Omit<SelectProps, "options"> ) {
  const [roles] = server.bot.useRoles();

  return (
    <Select options={roles ? roles.reduce((acc, role) => {acc[role.id] = (
      <span style={{ color: "#"+role.color.toString(16) }}>
        { role.name }
      </span>
    ); return acc; }, {}) : []} {...props} />
  );
}
