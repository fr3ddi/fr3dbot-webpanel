export default function Modal({ children, close }) {
  return (
    <div className="fixed inset-0 w-full h-full z-50 flex justify-center items-center bg-black bg-opacity-75" onClick={ close }>
      <div className="py-2 px-4 rounded bg-dark" onClick={ ev => ev.stopPropagation() }>
        { children }
      </div>
    </div>
  );
}
