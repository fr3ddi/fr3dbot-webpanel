import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import hljs from 'highlight.js/lib/core';
// import markdown from 'highlight.js/lib/languages/markdown';
import cn from "classnames";

// hljs.registerLanguage('markdown', markdown);

type InputProps = React.InputHTMLAttributes<HTMLInputElement|HTMLTextAreaElement> & {
  label?: string|JSX.Element,
  note?: string|JSX.Element,
  hideable?: boolean,
  forceValue?: boolean,
  hidden?: boolean,
  wrapClassName?: string,
};

export default function Input({label, note, className, wrapClassName, hideable, hidden, onChange, value, defaultValue, forceValue, ...props}: InputProps) {
  const [ _hidden, setHidden ] = useState(hidden ?? !(value || defaultValue));
  const [ _value, setValue ] = useState(value ?? defaultValue ?? "");

  useEffect(() => {
    setValue(value);
  }, [value]);

  const Element = props.type === "textarea" ? "textarea" : "input";

  const _hideable = hideable && label;

  return (
    <label onClick={ () => hideable && setHidden(!_hidden) } className={ cn(
      "inline-block leading-0",
      wrapClassName,
      _hideable ? "transition-all": "",
      _hideable && _hidden ? "-mb-2" : "",
      {"cursor-pointer": hideable},
    ) }>
      { label && <span className="block font-normal leading-relaxed pl-2">
        { label }
        { props.required && <span className="text-red-500">*</span> }
        { _hideable &&
          <FontAwesomeIcon icon={["fas", _hidden ? "angle-down" : "angle-up"]} className={cn("ml-2", { "text-gray-400": _hidden})} />
        }
      </span> }
      <Element value={ _value || "" }
        onClick={ ev => ev.stopPropagation() }
        onChange={ ({target}) => onChange && onChange(target.value) }
        onInput={ ({target}) => !forceValue && setValue(target.value) }
        className={cn(
        "bg-notblack w-full rounded overflow-hidden leading-tight font-light border-blackish h-11",
        props.readOnly ? "text-gray-300" : "border",
        props.type !== "color" ? "p-3" : "cursor-pointer",
        _hideable ? "transition-transform transform-gpu origin-top": "",
        _hideable && _hidden ? "scale-y-0 -mb-11" : "",
        className,
      )} {...props} />

      { note &&
        <span className={cn(
          "block font-normal leading-tight text-xs text-gray-400 mt-1 mb-3",
          _hideable ? "transition-all": "",
          _hideable && _hidden ? "-mt-3" : "",
        )}>{ note }</span>
      }
    </label>
  );
}
