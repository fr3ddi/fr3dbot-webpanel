import { useState } from "react";
import cn from "classnames";
import Image, { ImageProps } from "./Image";

interface OptionalImageProps extends ImageProps {
  width: string|number;
  height: string|number;
  toggle?: boolean|any;
  mediaClass?: string;
}

export default function OptionalImage({className, mediaClass, toggle, onLoad, ...props}: OptionalImageProps) {
  const [ loaded, setLoaded ] = useState(false);

  return (
    <div style={{ height: props.height, width: props.width }}
      className={cn("inline-block transition leading-0", className, !loaded && "bg-blackish")}
      >
      { toggle &&
        <Image {...props} className={mediaClass} onLoad={ () => { setLoaded(true); if (onLoad) onLoad(); } } />
      }
    </div>
  );
}
