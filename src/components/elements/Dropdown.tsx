import { useState, useEffect } from "react";
import cn from "classnames";

type DropdownProps = {
  trigger: JSX.Element,
  menu: (string|JSX.Element)[],
  className?: string,
  toLeft?: boolean,
};

export default function Dropdown({ trigger, menu, className, toLeft }: DropdownProps) {
  const [ open, setOpen ] = useState(false);

  useEffect(() => {
    const docListener = () => open && setOpen(false);
    window.addEventListener("click", docListener, { passive: true });

    return () => {
      window.removeEventListener("click", docListener);
    };
  }, [open]);

  return (
    <div className={cn("relative", className)} onClick={ () => setOpen(!open) }>
      { trigger }

      <ul className={cn(
        "absolute top-full bg-greyple py-2 rounded transition transform-gpu origin-top whitespace-nowrap",
        toLeft ? "right-0" : "left-0",
        !open ? "scale-y-0" : "",
      )}>
        { menu.map((item, i) =>
          <li key={ i } className={cn(
            "transition hover:bg-gray-500",
            typeof item === "string" ? "px-2 py-1" : "",
          )}>{ item }</li>
        ) }
      </ul>
    </div>
  );
}
