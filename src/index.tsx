import React from "react";
import ReactDOM from "react-dom";
import server from "lib/server";
import App from "./App";

server.init();

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
