module.exports = {
  purge: ["./src/**/*.{js,ts,jsx,tsx,html}"],
  darkMode: false, // 'media' or 'class'
  theme: {
    extend: {
      transitionProperty: {
        "rounded": "border-radius",
        "transform": "transform, margin",
      },
      colors: {
        link: "#0096d1",
        blurple: "#7289DA",
        greyple: "#99AAB5",
        dark: "#2C2F33",
        notblack: "#23272A",
        blackish: "#131416",
      },
      zIndex: {
        "-1": -1,
      },
      lineHeight: {
        "0": 0,
        "1/2": 0.5,
      },
      height: {
        "8.5": "2.125rem",
      },
      maxWidth: {
        "1/2": "50%",
        "1/3": "33%",
        "1/4": "25%",
        DEFAULT: "960px",
      },
    },
  },
  variants: {
    extend: {
      opacity: ["disabled"],
    }
  }
};
