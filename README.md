# WebPanel for Fr3dbot
A WebPanel for [Fr3dbot](https://bitbucket.org/fr3ddi/fr3dbot) using it's Socket API.

---

## To run this
- Install dependencies `npm i`
- Build `npm run build`
- And statically serve newly created `build/` folder on HTTP, serve `index.html` as a fallback when a file is not found
